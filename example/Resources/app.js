(function () {
  var tidm = require('cat.start.tidm');

  // use mobile data for downloads

  tidm.usesMobileData = true;

  // get downloads "ddbb"

  var downloads = tidm.downloads;

 for (var i = 0, l = downloads.length, download; i < l; i += 1) {
  download = downloads[i];

  /*
    type Download = {
      url: string;
      key: string;
      headers: { [key: string]: string ] } | null;
      meta: { [key: string]: string ] } | null;
      status: number;
      file?: string;
    }
  */

  if (download.status === tidm.STATUS_PROGRESS) {
    // type ProgressDownload = Download
  }
  else if (download.status === tidm.STATUS_COMPLETE) {
    // type CompleteDownload = Download & { file: string; }
  }
  else if (download.status === tidm.STATUS_FAILED) {
    // type FailedDownload = Download
  }
  else if (download.status === tidm.STATUS_PAUSED) {
    // type PausedDownload = Download
  }
  else if (download.status === tidm.STATUS_CANCELLED) {
    // type CancelledDownload = Download
  }
}


  // events

  tidm.addEventListener('change', function(ev) {
    // type Event = { key: string; index: number; kind: number; }

    if (ev.kind === tidm.EVENT_TYPE_PROGRESS) {
      // type ProgressEvent = Event & { progress: number; }
      Ti.API.info(ev.progress); // [0.0, 1.0]
    }
    else if (ev.kind === tidm.EVENT_TYPE_COMPLETE) {
      // type CompleteEvent = Event & { file: string; }
      Ti.API.info("event / complete");
    }
    else if (ev.kind === tidm.EVENT_TYPE_ERROR) {
      // type ErrorEvent = Event & { error: string; httpStatusCode: number; }
      Ti.API.info("event / error");
    }
    else if (ev.kind === tidm.EVENT_TYPE_PAUSE) {
      // type PauseEvent = Event
      Ti.API.info("event / pause");
    }
    else if (ev.kind === tidm.EVENT_TYPE_RESUME) {
      // type ResumeEvent = Event
      Ti.API.info("event / resume");
    }
    else if (ev.kind === tidm.EVENT_TYPE_CANCEL) {
      // type CancelEvent = Event
      Ti.API.info("event / cancel");
    }
    else if (ev.kind === tidm.EVENT_TYPE_DELETE) {
      // type DeleteEvent = Event
      Ti.API.info("event / delete");
    }
  });

  /*
   * methods
   */

  var key = (function() {
    var text = "",
      possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 10; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
  })();

  // start a download

  if (false) {
    // startDownload returns:
    // type StartDownloadResult = Download | undefined

    var newDownload = tidm.startDownload({
      'url': 'https://speed.hetzner.de/100MB.bin',
      'key': key,
      'headers': {
        'x-sct1': 'hey'
      },
      'meta': {
        'hellow': 'yellow'
      }
    });
    downloads.push(newDownload);
  }

  // pauseDownload: pauses a download
  // resumeDownload: resumes a download
  // cancelDownload: cancel a download
  // retryDownload: resumes a download that previously failed
  // deleteDownload: removes a cancelled or completed download from the "database"

  // every method here returns:
  // takes a single parameter with: type DownloadOperationRequest: { key: string; }
  // returns: type DownloadOperationResult = { error: boolean; type: number; }

  if (false) {
    var result = tidm.pauseDownload({
      'key': key
    });

    if (result.error) {
      if (result.type === tidm.ERROR_INVALID_ARGUMENTS) {
        Ti.API.info("pause error: invalid arguments");
      }
      else if (result.type === tidm.ERROR_INVALID_STATE) {
        Ti.API.info("pause error: invalid state");
      }
      return;
    }
  }

})();