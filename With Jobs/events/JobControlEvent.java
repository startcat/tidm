package cat.start.tidm.events;

public class JobControlEvent {
    public final String message;

    public JobControlEvent(String message) {
        this.message = message;
    }
}
