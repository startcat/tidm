package cat.start.tidm.jobs;

import android.support.annotation.NonNull;

//import com.evernote.android.job.Job;
//import com.evernote.android.job.JobRequest;
//import com.evernote.android.job.util.support.PersistableBundleCompat;

//import org.greenrobot.eventbus.EventBus;
//import org.greenrobot.eventbus.Subscribe;
//import org.greenrobot.eventbus.ThreadMode;

//import java.util.concurrent.CountDownLatch;

//import cat.start.tidm.events.JobControlEvent;

public class DownloadJob extends Job {

    public static final String TAG = "download_job_tag";

    private static final String EXTRA_URL = "url";

    //private final CountDownLatch countDownLatch = new CountDownLatch(1);

    private void finish() {
        //countDownLatch.countDown();
        //EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onControlEvent(JobControlEvent event) {
        //finish();
    }

	/*
    @Override
    @NonNull
    protected Result onRunJob(Params params) {
        EventBus.getDefault().register(this);

        try {
            countDownLatch.await();
        } catch (InterruptedException ignored) {}

        return Result.SUCCESS;
    }

    @Override
    protected void onCancel() {
        finish();
    }

    public static void makeJob(String url) {
        PersistableBundleCompat extras = new PersistableBundleCompat();

        new JobRequest.Builder(DownloadJob.TAG)
                .addExtras(extras)
                .startNow()
                .build()
                .schedule();
    }
    */
}