//
//  STCDownload.m
//  tidm
//
//  Created by yellow on 03/09/2019.
//

#import "STCDownload.h"

@interface STCDownload () <NSCoding>
@end

@implementation STCDownload

-(id)initWithCoder:(NSCoder*)decoder
{
    self = [super init];
    if (!self) {
        return nil;
    }
  
    _key = [decoder decodeObjectForKey:@"key"];
    _url = [decoder decodeObjectForKey:@"url"];
    _file = [decoder decodeObjectForKey:@"file"];
  
    _meta = [decoder decodeObjectForKey:@"meta"];
    _headers = [decoder decodeObjectForKey:@"headers"];
  
    _resumeData = [decoder decodeObjectForKey:@"resumeData"];
  
    _status = [decoder decodeIntegerForKey:@"status"];
  
  return self;
}

-(void)encodeWithCoder:(NSCoder*)encoder
{
    [encoder encodeObject:_key forKey:@"key"];
    [encoder encodeObject:_url forKey:@"url"];
    [encoder encodeObject:_file forKey:@"file"];
  
    [encoder encodeObject:_meta forKey:@"meta"];
    [encoder encodeObject:_headers forKey:@"headers"];
  
    [encoder encodeObject:_resumeData forKey:@"resumeData"];
  
    [encoder encodeInteger:_status forKey:@"status"];
}

-(NSDictionary*)json
{
  return @{@"key": _key,
           @"url": _url,
           @"file": _file ? _file : [NSNull null],
           @"meta": _meta ? _meta : [NSNull null],
           @"headers": _headers ? _headers : [NSNull null],
           @"status": [NSNumber numberWithInteger:_status]};
}

@end
