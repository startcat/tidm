/**
 * tidm
 *
 * Created by jordi domenech
 * Copyright (c) 2019 Your Company. All rights reserved.
 */

#import "CatStartTidmModule.h"
#import "TiBase.h"
#import "TiHost.h"
#import "TiUtils.h"

#import "HWIFileDownloader.h"
#import "STCDownload.h"

#pragma mark - Constants

static NSString* const kTiDmUserDefaultsKey = @"cat.start.tidm.downloads";

static NSString* const kTiDmEventName = @"change";
static NSString* const kTiDmEventPropertyType = @"kind";
static NSString* const kTiDmEventPropertyKey = @"key";
static NSString* const kTiDmEventPropertyIndex = @"index";

static NSInteger const kTiDmEventTypeProgress = 10;
static NSInteger const kTiDmEventTypeComplete = 11;
static NSInteger const kTiDmEventTypeError = 12;
static NSInteger const kTiDmEventTypePause = 13;
static NSInteger const kTiDmEventTypeResume = 14;
static NSInteger const kTiDmEventTypeCancelled = 15;
static NSInteger const kTiDmEventTypeDeleted = 16;

static NSInteger const kTiDmDownloadStatusProgress = 20;
static NSInteger const kTiDmDownloadStatusComplete = 21;
static NSInteger const kTiDmDownloadStatusError = 22;
static NSInteger const kTiDmDownloadStatusPaused = 23;
static NSInteger const kTiDmDownloadStatusCancelled = 24;

static NSInteger const kTiDmErrorInvalidArguments = 30;
static NSInteger const kTiDmErrorInvalidState = 31;
static NSInteger const kTiDmErrorNotPausable = 32;

#pragma mark - Private definition

@interface CatStartTidmModule () <HWIFileDownloadDelegate>

@property (nonatomic, strong) HWIFileDownloader* downloader;
@property (nonatomic, strong) NSMutableArray<STCDownload*>* downloads;

@end

#pragma mark - Implementation

@implementation CatStartTidmModule

#pragma mark helpers

-(nullable STCDownload*)storeSearchDownloadByKey:(nonnull NSString*)key
{
  NSPredicate* predicate = [NSPredicate predicateWithFormat:@"key == %@", key];
  NSArray* filteredArray = [_downloads filteredArrayUsingPredicate:predicate];
  return filteredArray.count ? filteredArray.firstObject : nil;
}

-(nullable STCDownload*)storeSearchDownloadByUrl:(nonnull NSString*)url
{
  NSPredicate* predicate = [NSPredicate predicateWithFormat:@"url == %@", url];
  NSArray* filteredArray = [_downloads filteredArrayUsingPredicate:predicate];
  return filteredArray.count ? filteredArray.firstObject : nil;
}

-(void)storeRetrieve
{
  NSData* data = [[NSUserDefaults standardUserDefaults] objectForKey:kTiDmUserDefaultsKey];
  if (!data) {
    _downloads = NSMutableArray.new;
    return;
  }
  _downloads = [NSKeyedUnarchiver unarchiveObjectWithData:data];
}

-(void)storePersist
{
  NSData* data = [NSKeyedArchiver archivedDataWithRootObject:_downloads];
  [[NSUserDefaults standardUserDefaults] setObject:data forKey:kTiDmUserDefaultsKey];
}

-(nonnull NSMutableDictionary*)makeEventWithType:(NSInteger)eventType forDownload:(nonnull STCDownload*)download
{
  NSMutableDictionary* event = NSMutableDictionary.dictionary;
  [event setObject:[NSNumber numberWithInteger:eventType]
            forKey:kTiDmEventPropertyType];
  [event setObject:[NSNumber numberWithUnsignedInteger:[_downloads indexOfObject:download]]
            forKey:kTiDmEventPropertyIndex];
  [event setObject:download.key
            forKey:kTiDmEventPropertyKey];
  return event;
}

#pragma mark HWIFileDownloadDelegate

-(nullable NSURLRequest*)urlRequestForRemoteURL:(nonnull NSURL*)remoteURL
{
  NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:remoteURL
                                                              cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                          timeoutInterval:60.0];
  STCDownload* download = [self storeSearchDownloadByUrl:remoteURL.absoluteString];
  if (download && download.headers) {
    for(NSString* key in download.headers) {
      NSString* value = [download.headers objectForKey:key];
      [request setValue:value forHTTPHeaderField:key];
    }
  }
  
  return request;
}

-(void)downloadProgressChangedForIdentifier:(nonnull NSString*)identifier
{
  STCDownload* download = [self storeSearchDownloadByKey:identifier];
  if (!download) {
    DebugLog(@"[ERROR] %@ is not a valid download key (progress)", identifier);
    return;
  }
  
  NSMutableDictionary* event = [self makeEventWithType:kTiDmEventTypeProgress
                                           forDownload:download];
  HWIFileDownloadProgress* downloadProgress = [_downloader downloadProgressForIdentifier:identifier];
  float progress = !downloadProgress ? 0 : downloadProgress.downloadProgress;
    int64_t current = !downloadProgress ? 0 : downloadProgress.receivedFileSize;
    int64_t total = !downloadProgress ? 0 : downloadProgress.expectedFileSize;

  [event setObject:[NSNumber numberWithFloat:progress]
            forKey:@"progress"];
    [event setObject:[NSNumber numberWithLong:current]
              forKey:@"current"];
    [event setObject:[NSNumber numberWithLong:total]
              forKey:@"total"];
  [self fireEvent:kTiDmEventName withObject:event];
}

-(void)downloadDidCompleteWithIdentifier:(nonnull NSString*)identifier
                            localFileURL:(nonnull NSURL*)localFileURL
{
  STCDownload* download = [self storeSearchDownloadByKey:identifier];
  if (!download) {
    DebugLog(@"[ERROR] %@ is not a valid download key (complete)", identifier);
    return;
  }

  NSMutableDictionary* event = [self makeEventWithType:kTiDmEventTypeComplete
                                           forDownload:download];
  download.file = localFileURL.absoluteString;
  download.status = kTiDmDownloadStatusComplete;
  [self storePersist];
  
  [event setObject:download.file
            forKey:@"file"];
  [self fireEvent:kTiDmEventName withObject:event];
}

-(void)downloadFailedWithIdentifier:(nonnull NSString*)identifier
                              error:(nonnull NSError*)error
                     httpStatusCode:(NSInteger)httpStatusCode
                 errorMessagesStack:(nullable NSArray<NSString*>*)errorMessagesStack
                         resumeData:(nullable NSData*)resumeData
{
  STCDownload* download = [self storeSearchDownloadByKey:identifier];
  if (!download) {
    DebugLog(@"[ERROR] %@ is not a valid download key (error)", identifier);
    return;
  }
  if (download.status == kTiDmDownloadStatusPaused) {
    download.resumeData = resumeData;
    [self storePersist];
  }
  else if (download.status == kTiDmDownloadStatusCancelled) {
  }
  else {
    download.status = kTiDmDownloadStatusError;
    [self storePersist];

    if (download.status == kTiDmDownloadStatusError) {
      NSMutableDictionary* event = [self makeEventWithType:kTiDmEventTypeComplete
                                               forDownload:download];
      [event setObject:error.description
                forKey:@"error"];
      [event setObject:[NSNumber numberWithInteger:httpStatusCode]
                forKey:@"httpStatusCode"];
      [self fireEvent:kTiDmEventName withObject:event];
    }
  }
}

-(void)downloadPausedWithIdentifier:(nonnull NSString*)identifier
                         resumeData:(nullable NSData*)resumeData
{
  STCDownload* download = [self storeSearchDownloadByKey:identifier];
  if (!download) {
    DebugLog(@"[ERROR] %@ is not a valid download key (pause)", identifier);
    return;
  }
  
  download.resumeData = resumeData;
  [self storePersist];
  
  NSMutableDictionary* event = [self makeEventWithType:kTiDmEventTypePause
                                           forDownload:download];
  [self fireEvent:kTiDmEventName withObject:event];
}

-(void)resumeDownloadWithIdentifier:(nonnull NSString*)identifier
{
  STCDownload* download = [self storeSearchDownloadByKey:identifier];
  if (!download) {
    DebugLog(@"[ERROR] %@ is not a valid download key (resume)", identifier);
    return;
  }
  
  NSMutableDictionary* event = [self makeEventWithType:kTiDmEventTypeResume
                                           forDownload:download];
  [self fireEvent:kTiDmEventName withObject:event];
}

-(void)cancelDownloadWithIdentifier:(nonnull NSString*)identifier;
{
  STCDownload* download = [self storeSearchDownloadByKey:identifier];
  if (!download) {
    DebugLog(@"[ERROR] %@ is not a valid download key (cancel)", identifier);
    return;
  }
  
  NSMutableDictionary* event = [self makeEventWithType:kTiDmEventTypeCancelled
                                           forDownload:download];
  [self fireEvent:kTiDmEventName withObject:event];
}

// TODO:
/*
-(void)customizeBackgroundSessionConfiguration:(nonnull NSURLSessionConfiguration*)backgroundSessionConfiguration
{
  backgroundSessionConfiguration.allowsCellularAccess = _mobileDataEnabled;
}
*/

-(void)incrementNetworkActivityIndicatorActivityCount {}
-(void)decrementNetworkActivityIndicatorActivityCount {}

#pragma mark titanium


-(id)moduleGUID
{
  return @"74f90bc8-a7c5-4385-9499-eae350c86a1d";
}

-(NSString*)moduleId
{
  return @"cat.start.tidm";
}

#pragma mark titanium / lifecycle

-(void)startup
{
  [super startup];
  
  [self storeRetrieve];
  
  // TODO: _mobileDataEnabled = NO;
  _downloader = [[HWIFileDownloader alloc] initWithDelegate:self];
}

#pragma mark titanium / js land

MAKE_SYSTEM_NUMBER(EVENT_TYPE_PROGRESS, NUMINT(kTiDmEventTypeProgress));
MAKE_SYSTEM_NUMBER(EVENT_TYPE_COMPLETE, NUMINT(kTiDmEventTypeComplete));
MAKE_SYSTEM_NUMBER(EVENT_TYPE_ERROR, NUMINT(kTiDmEventTypeError));
MAKE_SYSTEM_NUMBER(EVENT_TYPE_PAUSE, NUMINT(kTiDmEventTypePause));
MAKE_SYSTEM_NUMBER(EVENT_TYPE_RESUME, NUMINT(kTiDmEventTypeResume));
MAKE_SYSTEM_NUMBER(EVENT_TYPE_CANCEL, NUMINT(kTiDmEventTypeCancelled));
MAKE_SYSTEM_NUMBER(EVENT_TYPE_DELETE, NUMINT(kTiDmEventTypeDeleted));

MAKE_SYSTEM_NUMBER(STATUS_PROGRESS, NUMINT(kTiDmDownloadStatusProgress));
MAKE_SYSTEM_NUMBER(STATUS_COMPLETE, NUMINT(kTiDmDownloadStatusComplete));
MAKE_SYSTEM_NUMBER(STATUS_FAILED, NUMINT(kTiDmDownloadStatusError));
MAKE_SYSTEM_NUMBER(STATUS_PAUSED, NUMINT(kTiDmDownloadStatusPaused));
MAKE_SYSTEM_NUMBER(STATUS_CANCELLED, NUMINT(kTiDmDownloadStatusCancelled));

MAKE_SYSTEM_NUMBER(ERROR_INVALID_ARGUMENTS, NUMINT(kTiDmErrorInvalidArguments));
MAKE_SYSTEM_NUMBER(ERROR_INVALID_STATE, NUMINT(kTiDmErrorInvalidState));
MAKE_SYSTEM_NUMBER(ERROR_NOT_PAUSABLE, NUMINT(kTiDmErrorNotPausable));

-(NSArray<NSDictionary*>*)downloads
{
  NSMutableArray<NSDictionary*>* ret = NSMutableArray.new;
  for (STCDownload* download in _downloads) {
    if (download.status == kTiDmDownloadStatusProgress) {
      NSMutableDictionary* m_download = [NSMutableDictionary dictionaryWithDictionary:download.json];
      HWIFileDownloadProgress* downloadProgress = [_downloader downloadProgressForIdentifier:download.key];
      float progress = !downloadProgress ? 0 : downloadProgress.downloadProgress;
      [m_download setObject:[NSNumber numberWithFloat:progress]
                     forKey:@"progress"];
      [ret addObject:m_download];
    }
    else {
      [ret addObject:download.json];
    }
  }
  return ret;
}

-(nullable NSDictionary*)startDownload:(id)args
{
  ENSURE_SINGLE_ARG(args, NSDictionary);

  NSString* p_url = [TiUtils stringValue:@"url" properties:args def:nil];
  if (!p_url) {
    DebugLog(@"[ERROR] url is missing");
    return nil;
  }
  
  NSURL* url = [NSURL URLWithString:p_url];
  if (!url) {
    DebugLog(@"[ERROR] url is not valid");
    return nil;
  }
  
  NSString* key = [TiUtils stringValue:@"key" properties:args def:nil];
  if (!key) {
    DebugLog(@"[ERROR] key is missing");
    return nil;
  }
  
  if ([self storeSearchDownloadByKey:key]) {
    DebugLog(@"[ERROR] duplicated key");
    return nil;
  }
  
  STCDownload* download = STCDownload.new;
  download.key = key;
  download.url = p_url;
  download.status = kTiDmDownloadStatusProgress;
  download.meta = [args objectForKey:@"meta"];
  download.headers = [args objectForKey:@"headers"];
  [_downloads addObject:download];
  [self storePersist];
  
  [_downloader startDownloadWithIdentifier:download.key fromRemoteURL:url];
  
  return download.json;
}

-(nonnull NSDictionary*)pauseDownload:(id)args
{
  ENSURE_SINGLE_ARG(args, NSDictionary);
  
  NSString* key = [TiUtils stringValue:@"key" properties:args def:nil];
  if (!key) {
    DebugLog(@"[ERROR] key is missing");
    return @{@"error": [NSNumber numberWithBool:YES], @"type": [NSNumber numberWithInteger:kTiDmErrorInvalidArguments]};
  }
  
  STCDownload* download = [self storeSearchDownloadByKey:key];
  if (!download) {
    DebugLog(@"[ERROR] %@ is not a valid download key", key);
    return @{@"error": [NSNumber numberWithBool:YES], @"type": [NSNumber numberWithInteger:kTiDmErrorInvalidArguments]};
  }
  
  if (download.status != kTiDmDownloadStatusProgress) {
    DebugLog(@"[ERROR] %@ is not downloading (1)", key);
    return @{@"error": [NSNumber numberWithBool:YES], @"type": [NSNumber numberWithInteger:kTiDmErrorInvalidState]};
  }
  
  if (![_downloader isDownloadingIdentifier:download.key]) {
    DebugLog(@"[ERROR] %@ is not downloading (2)", key);
    return @{@"error": [NSNumber numberWithBool:YES], @"type": [NSNumber numberWithInteger:kTiDmErrorInvalidState]};
  }
  
  HWIFileDownloadProgress* progress = [_downloader downloadProgressForIdentifier:download.key];
  if (!progress) {
    DebugLog(@"[WARN] %@ won't resume since no native progress", key);
    return @{@"error": [NSNumber numberWithBool:YES], @"type": [NSNumber numberWithInteger:kTiDmErrorInvalidState]};
  }
  
  download.status = kTiDmDownloadStatusPaused;
  [self storePersist];

  [progress.nativeProgress pause];

  return @{@"error": [NSNumber numberWithBool:NO]};
}

-(NSDictionary*)resumeDownload:(id)args
{
  ENSURE_SINGLE_ARG(args, NSDictionary);
  
  NSString* key = [TiUtils stringValue:@"key" properties:args def:nil];
  if (!key) {
    DebugLog(@"[ERROR] key is missing");
    return @{@"error": [NSNumber numberWithBool:YES], @"type": [NSNumber numberWithInteger:kTiDmErrorInvalidArguments]};
  }
  
  STCDownload* download = [self storeSearchDownloadByKey:key];
  if (!download) {
    DebugLog(@"[ERROR] %@ is not a valid download key", key);
    return @{@"error": [NSNumber numberWithBool:YES], @"type": [NSNumber numberWithInteger:kTiDmErrorInvalidArguments]};
  }
  
  if (download.status != kTiDmDownloadStatusPaused) {
    DebugLog(@"[ERROR] %@ is not paused", key);
    return @{@"error": [NSNumber numberWithBool:YES], @"type": [NSNumber numberWithInteger:kTiDmErrorInvalidState]};
  }
  
  download.status = kTiDmDownloadStatusProgress;
  [self storePersist];

  if (download.resumeData) {
    [_downloader startDownloadWithIdentifier:download.key
                             usingResumeData:download.resumeData];
  }
  else {
    [_downloader startDownloadWithIdentifier:download.key
                               fromRemoteURL:[NSURL URLWithString:download.url]];
  }
  
  return @{@"error": [NSNumber numberWithBool:NO]};
}

-(NSDictionary*)retryDownload:(id)args
{
  ENSURE_SINGLE_ARG(args, NSDictionary);
  
  NSString* key = [TiUtils stringValue:@"key" properties:args def:nil];
  if (!key) {
    DebugLog(@"[ERROR] key is missing");
    return @{@"error": [NSNumber numberWithBool:YES],
             @"type": [NSNumber numberWithInteger:kTiDmErrorInvalidArguments]};
  }
  
  STCDownload* download = [self storeSearchDownloadByKey:key];
  if (!download) {
    DebugLog(@"[ERROR] %@ is not a valid download key", key);
    return @{@"error": [NSNumber numberWithBool:YES],
             @"type": [NSNumber numberWithInteger:kTiDmErrorInvalidArguments]};
  }
  
    /*
  if (download.status != kTiDmDownloadStatusError) {
    DebugLog(@"[ERROR] %@ has no error", key);
    return @{@"error": [NSNumber numberWithBool:YES],
             @"type": [NSNumber numberWithInteger:kTiDmErrorInvalidState]};
  }
    */
  
  download.status = kTiDmDownloadStatusProgress;
  [self storePersist];
  
  if (download.resumeData) {
    [_downloader startDownloadWithIdentifier:download.key
                             usingResumeData:download.resumeData];
  }
  else {
    [_downloader startDownloadWithIdentifier:download.key
                               fromRemoteURL:[NSURL URLWithString:download.url]];
  }
  
  return @{@"error": [NSNumber numberWithBool:NO]};
}

-(NSDictionary*)cancelDownload:(id)args
{
  ENSURE_SINGLE_ARG(args, NSDictionary);
  
  NSString* key = [TiUtils stringValue:@"key" properties:args def:nil];
  if (!key) {
    DebugLog(@"[ERROR] key is missing");
    return @{@"error": [NSNumber numberWithBool:YES],
             @"type": [NSNumber numberWithInteger:kTiDmErrorInvalidArguments]};
  }
  
  STCDownload* download = [self storeSearchDownloadByKey:key];
  if (!download) {
    DebugLog(@"[ERROR] %@ is not a valid download key", key);
    return @{@"error": [NSNumber numberWithBool:YES],
             @"type": [NSNumber numberWithInteger:kTiDmErrorInvalidArguments]};
  }
  
  if (download.status == kTiDmDownloadStatusCancelled) {
    DebugLog(@"[ERROR] %@ was previously cancelled", key);
    return @{@"error": [NSNumber numberWithBool:YES],
             @"type": [NSNumber numberWithInteger:kTiDmErrorInvalidState]};
  }
  
  download.status = kTiDmDownloadStatusCancelled;
  [self storePersist];
  
  [_downloader cancelDownloadWithIdentifier:download.key];

  return @{@"error": [NSNumber numberWithBool:NO]};
}

-(NSDictionary*)deleteDownload:(id)args
{
  ENSURE_SINGLE_ARG(args, NSDictionary);
  
  NSString* key = [TiUtils stringValue:@"key" properties:args def:nil];
  if (!key) {
    DebugLog(@"[ERROR] key is missing");
    return @{@"error": [NSNumber numberWithBool:YES],
             @"type": [NSNumber numberWithInteger:kTiDmErrorInvalidArguments]};
  }
  
  STCDownload* download = [self storeSearchDownloadByKey:key];
  if (!download) {
    DebugLog(@"[ERROR] %@ is not a valid download key", key);
    return @{@"error": [NSNumber numberWithBool:YES],
             @"type": [NSNumber numberWithInteger:kTiDmErrorInvalidArguments]};
  }
  
  if (download.status != kTiDmDownloadStatusComplete &&
      download.status != kTiDmDownloadStatusCancelled) {
    DebugLog(@"[ERROR] %@ is not cancelled or completed", key);
    return @{@"error": [NSNumber numberWithBool:YES],
             @"type": [NSNumber numberWithInteger:kTiDmErrorInvalidState]};
  }
  
  if (download.status == kTiDmDownloadStatusComplete || download.status == kTiDmDownloadStatusCancelled) {
    NSError* err;
    [[NSFileManager defaultManager] removeItemAtPath:download.file
                                               error:&err];
    if (err) {
      DebugLog(@"[ERROR] %@", err.description);
    }
  }
  
  [_downloads removeObject:download];
  [self storePersist];

  NSMutableDictionary* event = [self makeEventWithType:kTiDmEventTypeDeleted
                                           forDownload:download];
  [self fireEvent:kTiDmEventName withObject:event];
  
  return @{@"error": [NSNumber numberWithBool:NO]};
}

// TODO:
/*
-(NSNumber*)usesMobileData
{
  return [NSNumber numberWithBool:_mobileDataEnabled];
}

-(void)setUsesMobileData:(NSNumber*)usesMobileData
{
  BOOL value = usesMobileData.boolValue;
  if (value == _mobileDataEnabled) {
    return;
  }
  
  _mobileDataEnabled = value;
  [_downloader invalidateSessionConfigurationAndCancelTasks:YES];
}
*/

@end
