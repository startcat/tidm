//
//  STCDownload.h
//  tidm
//
//  Created by yellow on 03/09/2019.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface STCDownload : NSObject

@property (nonatomic, strong) NSString* key;
@property (nonatomic, strong) NSString* url;
@property (nonatomic, strong) NSString* file;

@property (nonatomic, strong) NSDictionary* meta;
@property (nonatomic, strong) NSDictionary* headers;

@property (nonatomic, assign) NSInteger status;

@property (nonatomic, strong) NSData* resumeData;

@property (readonly) NSDictionary* json;

@end

NS_ASSUME_NONNULL_END
