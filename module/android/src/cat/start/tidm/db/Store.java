package cat.start.tidm.db;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import cat.start.tidm.TidmModule;

// https://wiki.appcelerator.org/display/guides2/Android+Module+Architecture
// https://github.com/appcelerator-modules/ti.moddevguide/blob/c0fab749a7338ca31e3626a68beef1e451681ba2/android/src/ti/moddevguide/MethodsDemoProxy.java

public class Store {
    private static final String PREFS_NAME = "cat_start_tidm_shared_preferences";
    private static final String PREFS_DATA_KEY = "cat_start_tidm_shared_preferences_data";

    private SharedPreferences mSharedPreferences;
    private ArrayList<HashMap> mItems = new ArrayList<>();

    private Store(SharedPreferences sharedPreferences) {
        mSharedPreferences = sharedPreferences;

        String json = sharedPreferences.getString(PREFS_DATA_KEY, null);

        if (json != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                mItems = objectMapper.readValue(json, ArrayList.class);
            } catch (IOException e) {
                Log.e(TidmModule.LCAT, Log.getStackTraceString(e));
            }
        }
        else {
            mItems = new ArrayList<>();
        }
    }

    public ArrayList<HashMap> getDownloads() {
        return mItems;
    }

    public void persist() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            String json = mapper.writeValueAsString(mItems);

            SharedPreferences.Editor editor = mSharedPreferences.edit();
            editor.putString(PREFS_DATA_KEY, json);
            editor.commit();
        } catch (JsonProcessingException e) {
            Log.e(TidmModule.LCAT, Log.getStackTraceString(e));
        }
    }

    public HashMap findByKey(String key) {
        for (HashMap item : mItems) {
            if (item.get("key").equals(key)) {
                return item;
            }
        }
        return null;
    }

    public int findIndex(String key) {
        int ret = 0;
        for (HashMap item : mItems) {
            if (item.get("key").equals(key)) {
                return ret;
            }
            ret += 1;
        }
        return -1;
    }

    public static Store retrieve(Context context) {
        return new Store(context.getSharedPreferences(PREFS_NAME,  Context.MODE_PRIVATE));
    }
}
