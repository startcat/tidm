# tidm module

a native downloads manager module for titanium.

## dependecies

- iOS: https://github.com/Heikowi/HWIFileDownload
- android: https://github.com/tonyofrancis/fetch

## project organization

- `example/` example app
- `example/native_xcode` **xcode** native project
- `example/native_as` **android studio** native project
- `module/` titanium modules (ios + android)

## common tasks

- first of all, we need to build the module for each platform: `ti build -p iphone -b` and `ti build -p android -b`. this is going to generate some `c++` and `javascript` _binding_ to let any titanium app using the module how to to require and use it. once the module has been built, we need to unzip the zip inside `dist/` and copy the module titanium bundle for each platform inside the example `modules/` directory.

- to build the example app, first edit `tiapp.xml` and the module. we made some special `cli script` files which add additional actions after building the titanium app. those files are inside `example/`: `prepare_ios` and `prepare_android`.

- we also need to rebuild the module / the app whenever the module's api changes / we make some change to the example's `app.js`.

## ios

- for the xcode project, we first build the app using the script `example/prepare_ios` and then we copy all the contents inside `build/iphone` except for `build/`, `build-manifest.json` and `DerivedData/`. this is going to avoid titanium cli destroys whatever changes we do to the project whenever we rebuild the app.

- in xcode, add (alt + cmd + A) the folder `native_xcode/js` selecting `Create groups`. this step was **the only required one** to make the app work using the plain generated xcode project!

next steps are required in order to being able to edit the module sources and run the app for testing!

- remove the references to our module inside the project. for `tidm` we needed to remove `libcat.start.tidm.a` static library from `tidmexample (target) / Build Phases / Link Binary With Libraries`.

- drag our module xcode project (`module/ios/tidm.xcodeproj`) in our app xcode project (`example/native_xcode/tidmexample.xcodeproj`)

- add `tidm` in  `tidmexample (target) / Build Phases / Target Dependencies`.

- add `Workspace / libcat.start.tidm.a` static library in `tidmexample (target) / Build Phases / Link Binary With Libraries`.

## android

- in AS remember to disable **instant run**!

- we can edit the ti SDK version in `example/native/build.gradle` (`ext / tiHome` variable)

**each time we build the example app** edit the AndroidManifest in AS:

- remove:
  `<uses-sdk android:minSdkVersion="..." android:targetSdkVersion="..."/>`

- add to application level tag:
  `tools:replace="android:name,android:label"`

- i don't know how it's added, but we need to remove also this:
  `<meta-data android:name="com.google.android.gms.version" android:value="@integer/google_play_services_version"/>`

for apps using this module we need to add this in `tiapp.xml`:

```
<android xmlns:android="http://schemas.android.com/apk/res/android">
  <manifest xmlns:tools="http://schemas.android.com/tools"
    xmlns:android="http://schemas.android.com/apk/res/android">
    <application tools:replace="android:name,android:label" />
  </manifest>
</android>
```

- we keep ti's `android-support-multidex.jar` _special_ version in `example/native/app/libs/` since it's really messy to keep only that library with gradle.
